<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Player $player
 */

?>

<div class="row">
    <div class="col px-4">
        <h1 class="display-5 my-4">Register Your Character</h1> 
    </div>
</div
<div class="row">
    <div class="col pb-4">
        <?= $this->Form->create($player) ?>

        <div id="player-details">
            <h3>Player Details</h3>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <?= $this->Form->control('irl_name', [
                            'class' => 'form-control',
                            'label' => 'IRL Name',
                        ]) ?>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <?= $this->Form->control('irl_location', [
                            'class' => 'form-control',
                            'label' => 'IRL Location',
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <?= $this->Form->control('dob', [
                            'class' => 'form-control',
                            'label' => 'Date of Birth',
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="char-details">
            <h3>Character Details</h3>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <?= $this->Form->control('character_name', [
                            'class' => 'form-control',
                            'label' => 'Character Name',
                        ]) ?>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <?= $this->Form->control('character_class', [
                            'type' => 'select',
                            'options' => [
                                'Orc' => 'Orc',
                                'Barbarian' => 'Barbarian',
                                'Fighter' => 'Fighter',
                                'Rogue' => 'Rogue',
                                'Thief' => 'Thief',
                            ],
                            'empty' => '--- Please select ---',
                            'class' => 'form-control',
                            'label' => 'Character Class',
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                <div class="form-group">
                        <?= $this->Form->control('bio', [
                            'class' => 'form-control',
                            'label' => 'Character Bio',
                        ]) ?>
                    </div> 
                </div>
            </div>
        </div>

        <div id="user-details">
            <h3>User Details</h3>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <?= $this->Form->control('username', [
                            'class' => 'form-control',
                            'label' => 'Username',
                        ]) ?>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <?= $this->Form->control('password', [
                            'class' => 'form-control',
                            'label' => 'Password',
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
                        
        <?= $this->Form->button(__('Start Playing Now!'), ['class' => 'btn btn-success']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>