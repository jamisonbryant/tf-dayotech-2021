<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Player[]|\Cake\Collection\CollectionInterface $players
 */

$this->assign('title', 'Players Online');
?>

<div class="row">
    <div class="col px-4">
        <h1 class="display-3 my-4">Players Online</h1> 
    </div>
</div>
<div class="row pb-2">
    <div class="col px-4">
        <table class="table table-dark">
            <tr>
                <th>Player ID</th>
                <th>Character Name</th>
                <th>Character Class</th>
                <th>IRL Location</th>
                <th>XP</th>
                <th>Gold</th>
                <th>Join Date</th>
            </tr>
            <?php if (count($players) > 0): ?>
            <?php foreach ($players as $player): ?>
            <tr>
                <td><?= $player->id ?></td>
                <td><?= $player->character_name ?></td>
                <td><?= $player->character_class ?></td>
                <td><?= $player->irl_location ?></td>
                <td><?= $this->Number->format($player->xp_points) ?></td>
                <td><?= $this->Number->format($player->gold_coins) ?></td>
                <td><?= $this->Time->nice($player->created) ?></td>
            </tr>
            <?php endforeach; ?>
            <?php else: ?>
            <tr>
                <td colspan="7">
                    <div class="text-center">
                        <p class="lead mt-3">No players online at the moment.</p>
                        <a href="/players/add" class="btn btn-link text-warning">Register your character now!</a>
                    </div>
                </td>
            </tr>
            <?php endif; ?>
        </table>
    </div>
</div>
