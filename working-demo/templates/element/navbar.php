<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand font-cinzel" href="#">Algolia Online&trade;</a>
    <div class="collapse navbar-collapse" id="navbarColor01">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active mx-2">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item mx-2">
                <a class="nav-link" href="/players">Players Online</a>
            </li>
            <li class="nav-item mx-2">
                <a class="nav-link" href="#">Pricing</a>
            </li>
            <li class="nav-item mx-2">
                <a class="nav-link" href="#">About</a>
            </li>
            <li class="nav-item mx-2">
                <a class="nav-link text-warning" href="/players/add">Register Now!</a>
            </li>
        </ul>
    </div>
</nav>