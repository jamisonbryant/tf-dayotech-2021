<!DOCTYPE html>

<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> <?= $this->fetch('title') ?> </title>
        <?= $this->Html->meta('icon') ?>

        <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

        <?= $this->Html->css(['site', 'bootstrap/bootstrap.min', 'fonts']) ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body>
        <?= $this->element('navbar') ?>
        <div class="container">
            <?= $this->fetch('content') ?>
        </div>
    </body>
</html>
